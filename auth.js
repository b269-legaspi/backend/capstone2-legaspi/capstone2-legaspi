const jwt = require("jsonwebtoken");

const secret = "super-long-secret-secret-secret-password";

module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  return jwt.sign(data, secret, {});
};

module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization;
  if (typeof token !== "undefined") {
    // console.log(token);
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return res.send({
          auth: "Failed. Please log in again.",
        });
      } else {
        next();
      }
    });
  } else {
    return res.send({ auth: "You are not logged in. Please log in." });
  }
};

module.exports.decode = (token) => {
  if (typeof token !== "undefined") {
    // console.log(token);
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        return jwt.decode(token, { complete: true }).payload;
      }
    });
  } else {
    return null;
  }
};
