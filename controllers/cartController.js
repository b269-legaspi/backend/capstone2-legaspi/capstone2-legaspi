// const cartProduct = require("../models/cartProduct");
const Cart = require("../models/Cart");
const Product = require("../models/Product");
const Order = require("../models/Order");

// Non-admin User add to cart
module.exports.addToCart = async (data, reqBody) => {
  try {
    const productDetails = await Product.findById(reqBody.productId);
    const cart = await Cart.findOne({
      "user.userId": data.id,
    });

    if (data.isAdmin == false) {
      if (cart) {
        const indexFound = cart.cartProducts.findIndex(
          (obj) => obj.productId == reqBody.productId
        );
        if (indexFound == -1 && reqBody.quantity != 0) {
          // if product does NOT exist yet in the cart AND reqBody.quantity is NOT set to 0..
          cart.cartProducts.push({
            productId: reqBody.productId,
            name: productDetails.name,
            description: productDetails.description,
            price: productDetails.price,
            quantity: parseInt(reqBody.quantity),
            subTotal: productDetails.price * reqBody.quantity,
            imgLink: productDetails.imgLink,
          });
          cart.totalAmount = cart.cartProducts
            .map((item) => {
              // console.log(item.subTotal);
              return item.subTotal;
              // item.subTotal.reduce((acc, next) => acc + next);
            })
            .reduce((acc, next) => acc + next, 0);
          // console.log(cart.totalAmount);
          cart.lastModifiedOn = new Date();
          cart.lastModifiedOnUTC = new Date();
          cart.save().then((cart, error) => {
            if (error) {
              return false;
            } else {
              return true;
            }
          });
          let message = Promise.resolve(
            `Product added to cart! Product name: ${productDetails.name}`
          );
          return message.then((value) => {
            return { value };
          });
          /////
        } else if (indexFound == -1 && reqBody.quantity == 0) {
          // if product does NOT exist yet in the cart AND reqBody.quantity is set to 0..
          let message = Promise.resolve(
            `You have no existing product with this product ID yet. Please input quantity. Product name: ${productDetails.name}`
          );
          return message.then((value) => {
            return { value };
          });
        }
        /////
        else if (indexFound != -1 && reqBody.quantity == 0) {
          // If product exists and quantity is set to 0
          // remove cartProductSchema from cartProducts array -- splice
          cart.cartProducts.splice(indexFound, 1);
          cart.totalAmount = cart.cartProducts
            .map((item) => item.subTotal)
            .reduce((acc, next) => acc + next, 0);
          cart.lastModifiedOn = new Date();
          cart.lastModifiedOnUTC = new Date();
          cart.save().then((cart, error) => {
            if (error) {
              return false;
            } else {
              return true;
            }
          });
          let message = Promise.resolve(
            `Product removed to cart! Product name: ${productDetails.name}`
          );
          return message.then((value) => {
            return { value };
          });
        } else if (indexFound != -1 && reqBody.quantity != 0) {
          // If product exists and quantity is not equal to zero
          // EDIT the previous quantity with the new quantity
          cart.cartProducts[indexFound].quantity = parseInt(reqBody.quantity);
          cart.cartProducts[indexFound].subTotal =
            productDetails.price * reqBody.quantity;
          Cart.findOneAndUpdate(
            { "user.userId": data.id },
            {
              $set: {
                [`cartProducts.${indexFound}.quantity`]: parseInt(
                  reqBody.quantity
                ), //$toInt:
                [`cartProducts.${indexFound}.subTotal`]:
                  productDetails.price * parseInt(reqBody.quantity),
              },
            }
          ).then((cart, error) => {
            if (error) {
              return false;
            } else {
              return true;
            }
          });
          cart.totalAmount = cart.cartProducts
            .map((item) => item.subTotal)
            .reduce((acc, next) => acc + next, 0);
          cart.lastModifiedOn = new Date();
          cart.lastModifiedOnUTC = new Date();
          cart.save().then((cart, error) => {
            if (error) {
              return false;
            } else {
              return true;
            }
          });
        }
        let message = Promise.resolve(
          `Product quantity has been changed! Product name: ${productDetails.name}`
        );
        return message.then((value) => {
          return { value };
        });
      } else {
        // if no cart exists yet
        let newCart = new Cart({
          user: {
            userId: data.id,
            email: data.email,
          },
          cartProducts: [
            {
              productId: reqBody.productId,
              name: productDetails.name,
              description: productDetails.description,
              price: productDetails.price,
              quantity: parseInt(reqBody.quantity),
              subTotal: productDetails.price * reqBody.quantity,
              imgLink: productDetails.imgLink,
            },
          ],
          totalAmount: productDetails.price * reqBody.quantity,
        });

        newCart.save().then((cart, error) => {
          if (error) {
            return false;
          } else {
            return true;
          }
        });

        let message = Promise.resolve("New Cart created");
        return message.then((value) => {
          return { value };
        });
      }
    } else {
      let message = Promise.resolve("User must NOT be admin to add to cart");
      return message.then((value) => {
        return { value };
      });
    }
  } catch {
    let message = Promise.resolve("Invalid Product ID");
    return message.then((value) => {
      return { value };
    });
  }
};

// Non-admin User Check Out Cart
module.exports.checkOutCart = async (data) => {
  try {
    if (!data.isAdmin) {
      // Getting the user's cart information first
      const cart = await Cart.findOne({
        "user.userId": data.id,
      });
      console.log(cart);

      if (cart.cartProducts.length == 0) {
        let message = Promise.resolve(
          "Your cart is empty. Please add products to cart before checking out"
        );
        return message.then((value) => {
          return { value };
        });
      } else {
        let newOrder = new Order({
          user: {
            userId: data.id,
            email: data.email,
          },
          products: cart.cartProducts,
          totalAmount: cart.totalAmount,
        });

        newOrder.save().then((cart, error) => {
          if (error) {
            return false;
          } else {
            return true;
          }
        });

        Cart.findOneAndUpdate(
          {
            "user.userId": data.id,
          },
          {
            cartProducts: [],
            totalAmount: 0,
            lastModifiedOn: new Date(),
            lastModifiedOnUTC: new Date(),
          }
        ).then((result) => {
          if (!result) {
            return false;
          } else {
            return true;
          }
        });

        let message = Promise.resolve("Order added successfully");
        return message.then((value) => {
          return { value };
        });
      }
    } else {
      let message = Promise.resolve("User must NOT be admin to add order");
      return message.then((value) => {
        return { value };
      });
    }
  } catch {
    let message = Promise.resolve("No cart found");
    return message.then((value) => {
      return { value };
    });
  }
};

// Retrieve authenticated user’s cart
module.exports.getUserCart = async (data) => {
  if (data.isAdmin == false) {
    return (
      Cart.find({ "user.userId": data.id })
        .select({
          user: 0,
          _id: 0, // id of cart is not important, finding cart is based on userId
          lastModifiedOnUTC: 0,
          __v: 0,
        })
        .then((result) => {
          if (result.length == 0) {
            let message = Promise.resolve("You have no existing cart.");
            return message.then((value) => {
              return { value };
            });
          } else {
            return result;
          }
        })
        // .catch(() => `Retrieving cart failed`);
        .catch(() => {
          let message = Promise.resolve("Retrieving cart failed");
          return message.then((value) => {
            return { value };
          });
        })
    );
  } else {
    let message = Promise.resolve(
      "User must NOT be admin to retrieve user cart"
    );
    return message.then((value) => {
      return { value };
    });
  }
};

// Retrieve all cart (Admin only)
module.exports.getAllCart = async (data) => {
  if (data.isAdmin) {
    return Cart.find({})
      .select({
        _id: 0, // id of cart is not important, finding cart is based on userId
        lastModifiedOnUTC: 0,
        __v: 0,
      })
      .then((result) => {
        if (result.length == 0) {
          return { value: "No existing cart." };
        } else {
          return result;
        }
      })
      .catch(() => {
        let message = Promise.resolve("Retrieving cart failed");
        return message.then((value) => {
          return { value };
        });
      });
  } else {
    let message = Promise.resolve("User must be ADMIN to access this");
    return message.then((value) => {
      return { value };
    });
  }
};

// Retrieve cart of SPECIFIC USERID (Admin only)
module.exports.getCartSpecificAdmin = async (data, reqParams) => {
  if (data.isAdmin) {
    return Cart.find({ "user.userId": reqParams.userId })
      .select({
        _id: 0, // id of cart is not important, finding cart is based on userId
        lastModifiedOnUTC: 0,
        __v: 0,
      })
      .then((result) => {
        if (result.length == 0) {
          return { value: "No existing cart for this user." };
        } else {
          return result;
        }
      })
      .catch(() => {
        let message = Promise.resolve("Retrieving cart failed");
        return message.then((value) => {
          return { value };
        });
      });
  } else {
    let message = Promise.resolve("User must be ADMIN to access this");
    return message.then((value) => {
      return { value };
    });
  }
};
