const Product = require("../models/Product");

// Create Product (Admin only)
module.exports.addProduct = (data, reqBody) => {
  // User is an admin
  if (data.isAdmin) {
    let newProduct = new Product({
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price,
      imgLink: reqBody.imgLink,
    });

    return newProduct.save().then((product, error) => {
      if (error) {
        return { value: "Product creation failed" };
      } else {
        return { value: "Product creation successful" };
      }
    });
  }
  // User is not an admin
  let message = Promise.resolve("User must be ADMIN to access this");
  return message.then((value) => {
    return { value };
  });
};

// Retrieve all active products
module.exports.getAllActiveProducts = () => {
  return Product.find({ isActive: true })
    .select({
      isActive: 0,
      createdOn: 0,
      __v: 0,
    })
    .then((result) => {
      return result;
    });
};

// Retrieve single product
module.exports.getProduct = (reqParams) => {
  return Product.findById(reqParams.productId)
    .select({
      isActive: 0,
      createdOn: 0,
      __v: 0,
    })
    .then((result) => {
      return result;
    })
    .catch(() => {
      let message = Promise.resolve(
        `${reqParams.productId} is an invalid Product ID`
      );
      return message.then((value) => {
        return { value };
      });
    });
};

// Update Product information (Admin only)
module.exports.updateProduct = (data, reqParams, reqBody) => {
  if (data.isAdmin) {
    let updatedProduct = {
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price,
      // added for capstone 3
      isActive: reqBody.isActive,
      imgLink: reqBody.imgLink,
    };

    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
      .then((product, error) => {
        if (error) {
          return { value: "Product update failed" };
        } else {
          return { value: "Product update successful" };
        }
      })
      .catch(() => {
        let message = Promise.resolve(
          `${reqParams.productId} is an invalid Product ID`
        );
        return message.then((value) => {
          return { value };
        });
      });
  }
  // User is not an admin
  let message = Promise.resolve("User must be ADMIN to access this");
  return message.then((value) => {
    return { value };
  });
};

// Archive Product (Admin only)
module.exports.archiveProduct = (data, reqParams) => {
  if (data.isAdmin) {
    let archivedProduct = {
      isActive: false,
    };

    return Product.findByIdAndUpdate(reqParams.productId, archivedProduct)
      .then((product, error) => {
        if (error) {
          return false;
        } else {
          return true;
        }
      })
      .catch(() => {
        let message = Promise.resolve(
          `${reqParams.productId} is an invalid Product ID`
        );
        return message.then((value) => {
          return { value };
        });
      });
  }
  // User is not an admin
  let message = Promise.resolve("User must be ADMIN to access this");
  return message.then((value) => {
    return { value };
  });
};

// added for capstone 3
// Retrieve all active products for admin
module.exports.getAllProducts = () => {
  return Product.find()
    .select({
      __v: 0,
    })
    .then((result) => {
      return result;
    });
};

module.exports.reactivateProduct = (data, reqParams) => {
  if (data.isAdmin) {
    let archivedProduct = {
      isActive: true,
    };

    return Product.findByIdAndUpdate(reqParams.productId, archivedProduct)
      .then((product, error) => {
        if (error) {
          return false;
        } else {
          return true;
        }
      })
      .catch(() => {
        let message = Promise.resolve(
          `${reqParams.productId} is an invalid Product ID`
        );
        return message.then((value) => {
          return { value };
        });
      });
  }
  // User is not an admin
  let message = Promise.resolve("User must be ADMIN to access this");
  return message.then((value) => {
    return { value };
  });
};
