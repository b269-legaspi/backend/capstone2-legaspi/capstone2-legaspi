const bcrypt = require("bcrypt");

const User = require("../models/User");
const auth = require("../auth");

// User registration
module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
  });
  return newUser.save().then((user, error) => {
    if (error) {
      return { value: "Registration failed" };
    } else {
      return { value: "Successfully Registered" };
    }
  });
};

// User authentication
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then((result) => {
      // User does not exist
      if (result == null) {
        return { value: "User does not exist" };
        // User exists
      } else {
        const isPasswordCorrect = bcrypt.compareSync(
          reqBody.password,
          result.password
        );
        // If the passwords match/result of the above code is true
        if (isPasswordCorrect) {
          return { access: auth.createAccessToken(result) };
          // If password do not match
        } else {
          return { value: "Password is incorrect" };
        }
      }
    })
    .catch(() => {
      let message = Promise.resolve("Login failed");
      return message.then((value) => {
        return { value };
      });
    });
};

// Retrieve User Details (Authenticated users only)
module.exports.getProfile = (data) => {
  return User.findById(data.userId)
    .select({ password: 0, __v: 0 })
    .then((result) => {
      return result;
    })
    .catch(() => {
      let message = Promise.resolve("Retrieving user details failed");
      return message.then((value) => {
        return { value };
      });
    });
};

// Set user as admin (Admin only)
module.exports.setUserToAdmin = (data, reqParams) => {
  if (data.isAdmin) {
    let userToAdmin = {
      isAdmin: true,
    };

    return User.findByIdAndUpdate(reqParams.userId, userToAdmin)
      .then((user, error) => {
        if (error) {
          // return `Promotion failed`;
          return false;
        } else {
          // return `${user.email} is promoted to admin`;
          return true;
        }
      })
      .catch(() => {
        let message = Promise.resolve(
          `${reqParams.userId} is an invalid User ID`
        );
        return message.then((value) => {
          return { value };
        });
      });
  }
  // User is not an admin
  let message = Promise.resolve("User must be ADMIN to access this");
  return message.then((value) => {
    return { value };
  });
};

// added for capstone 3
module.exports.checkEmailExists = (reqBody) => {
  // The result is sent back to the Postman via the "then" method found in the route file
  return User.find({ email: reqBody.email }).then((result) => {
    // The "find" method returns a record if a match is found
    if (result.length > 0) {
      return true;
      // No duplicate email found
      // The user is not yet registered in the database
    } else {
      return false;
    }
  });
};

// Change user/admin password
module.exports.changePassword = (data, reqBody) => {
  const { currentPassword, newPassword } = reqBody;

  if (currentPassword === newPassword) {
    let message = Promise.resolve("Please provide a new password");
    return message.then((value) => {
      return { value };
    });
  }

  if (data) {
    return User.findOne({ _id: data.id }).then((user) => {
      if (!user) {
        return false;
      } else {
        const isMatch = bcrypt.compareSync(currentPassword, user.password);

        if (isMatch) {
          user.password = bcrypt.hashSync(newPassword, 10);
          return user.save().then((user, error) => {
            if (error) {
              return false;
            } else {
              return true;
            }
          });
        } else {
          return { value: "Current password is incorrect" };
        }
      }
    });
  } else {
    let message = Promise.resolve("Please login to change password");
    return message.then((value) => {
      return { value };
    });
  }
};

// Retrieve all users (Admin only)
module.exports.retrieveAllUsers = (data) => {
  if (data.isAdmin) {
    let userToAdmin = {
      isAdmin: true,
    };

    return User.find({})
      .select({ password: 0, __v: 0 })
      .then((result, error) => {
        if (error) {
          return false;
        } else {
          return result;
        }
      })
      .catch(() => {
        let message = Promise.resolve(
          `${reqParams.userId} is an invalid User ID`
        );
        return message.then((value) => {
          return { value };
        });
      });
  }
  // User is not an admin
  let message = Promise.resolve("User must be ADMIN to access this");
  return message.then((value) => {
    return { value };
  });
};

// Demote admin to user (Admin only)
module.exports.demoteAdminToUser = (data, reqParams) => {
  if (data.isAdmin) {
    let userToAdmin = {
      isAdmin: false,
    };

    return User.findByIdAndUpdate(reqParams.userId, userToAdmin)
      .then((user, error) => {
        if (error) {
          // return `Promotion failed`;
          return false;
        } else {
          // return `${user.email} is promoted to admin`;
          return true;
        }
      })
      .catch(() => {
        let message = Promise.resolve(
          `${reqParams.userId} is an invalid User ID`
        );
        return message.then((value) => {
          return { value };
        });
      });
  }
  // User is not an admin
  let message = Promise.resolve("User must be ADMIN to access this");
  return message.then((value) => {
    return { value };
  });
};
