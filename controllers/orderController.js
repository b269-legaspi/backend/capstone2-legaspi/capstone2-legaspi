const Order = require("../models/Order");
const Product = require("../models/Product");

// Non-admin User checkout (Create Order)
module.exports.orderProduct = async (data, reqBody) => {
  if (data.isAdmin == false) {
    try {
      const subTotals = [];
      const productsArr = [];
      for (let i = 0; i < reqBody.length; i++) {
        const subTotal = await Product.findOne({
          _id: reqBody[i].productId,
        }).then((res) => {
          return res.price * reqBody[i].quantity;
        });
        subTotals.push(subTotal);

        const productDetails = await Product.findOne({
          _id: reqBody[i].productId,
        });
        productsArr.push({
          productId: productDetails._id,
          name: productDetails.name,
          description: productDetails.description,
          price: productDetails.price,
          quantity: reqBody[i].quantity,
          subTotal: productDetails.price * reqBody[i].quantity,
          imgLink: productDetails.imgLink,
        });
      }
      const totalAmount = subTotals.reduce((a, b) => a + b);

      let newOrder = new Order({
        user: {
          userId: data.id,
          email: data.email,
        },
        products: productsArr,
        // subTotals: subTotals,
        totalAmount: totalAmount,
      });

      newOrder.save().then((user, error) => {
        if (error) {
          return false;
        } else {
          return true;
        }
      });

      let message = Promise.resolve("Order added successfully");
      return message.then((value) => {
        return { value };
      });
    } catch {
      let message = Promise.resolve("Invalid Product ID");
      return message.then((value) => {
        return { value };
      });
    }
  } else {
    let message = Promise.resolve("User must NOT be admin to add order");
    return message.then((value) => {
      return { value };
    });
  }
};

// Retrieve authenticated user’s orders
module.exports.getUserOrder = async (data) => {
  if (data.isAdmin == false) {
    return Order.find({ "user.userId": data.id, isActive: true })
      .sort({ $natural: -1 })
      .select({
        user: 0,
        purchasedOnUTC: 0,
        isActive: 0,
        __v: 0,
      })
      .then((result) => {
        // if (result.length == 0) {
        //   return `You have no existing orders.`;
        // } else {
        return result;
        // }
      })
      .catch(() => `Retrieving order failed`);
  } else {
    let message = Promise.resolve(
      "User must NOT be admin to retrieve user orders"
    );
    return message.then((value) => {
      return { value };
    });
  }
};

// Retrieve authenticated user’s LATEST order
module.exports.getUserLatestOrder = async (data) => {
  if (data.isAdmin == false) {
    return Order.find({ "user.userId": data.id, isActive: true })
      .sort({ $natural: -1 })
      .limit(1)
      .select({
        user: 0,
        purchasedOnUTC: 0,
        isActive: 0,
        __v: 0,
      })
      .then((result) => {
        if (result.length == 0) {
          return { value: "You have no existing orders." };
        } else {
          return result;
        }
      })
      .catch(() => {
        let message = Promise.resolve("Retrieving order failed");
        return message.then((value) => {
          return { value };
        });
      });
  } else {
    let message = Promise.resolve(
      "User must NOT be admin to retrieve user orders"
    );
    return message.then((value) => {
      return { value };
    });
  }
};

// Retrieve authenticated user’s SPECIFIC order
module.exports.getUserSpecificOrder = async (data, reqParams) => {
  if (data.isAdmin == false) {
    return Order.find({
      "user.userId": data.id,
      _id: reqParams.orderId,
      isActive: true,
    })
      .select({
        user: 0,
        purchasedOnUTC: 0,
        isActive: 0,
        __v: 0,
      })
      .then((result) => {
        if (result.length == 0) {
          return {
            value: `Order with ID ${reqParams.orderId} does not exist`,
          };
        } else {
          return result;
        }
      })
      .catch(() => {
        let message = Promise.resolve(
          `${reqParams.orderId} is an invalid Order ID`
        );
        return message.then((value) => {
          return { value };
        });
      });
  } else {
    let message = Promise.resolve(
      "User must NOT be admin to retrieve user orders"
    );
    return message.then((value) => {
      return { value };
    });
  }
};

// Retrieve all orders (Admin only)
module.exports.getAllOrders = async (data) => {
  if (data.isAdmin) {
    return Order.find({ isActive: true })
      .sort({ $natural: -1 })
      .select({
        purchasedOnUTC: 0,
        isActive: 0,
        __v: 0,
      })
      .then((result) => {
        if (result.length == 0) {
          return { value: "No existing orders" };
        } else {
          return result;
        }
      })
      .catch(() => {
        let message = Promise.resolve("Retrieving order failed");
        return message.then((value) => {
          return { value };
        });
      });
  } else {
    let message = Promise.resolve("User must be ADMIN to access this");
    return message.then((value) => {
      return { value };
    });
  }
};

// Retrieve all orders of SPECIFIC USERID (Admin only)
module.exports.getAllOrdersSpecificAdmin = async (data, reqParams) => {
  if (data.isAdmin) {
    return Order.find({ "user.userId": reqParams.userId, isActive: true })
      .sort({ $natural: -1 })
      .select({
        purchasedOnUTC: 0,
        isActive: 0,
        __v: 0,
      })
      .then((result) => {
        if (result.length == 0) {
          return { value: "No existing orders for this user." };
        } else {
          return result;
        }
      })
      .catch(() => {
        let message = Promise.resolve("Retrieving order failed");
        return message.then((value) => {
          return { value };
        });
      });
  } else {
    let message = Promise.resolve("User must be ADMIN to access this");
    return message.then((value) => {
      return { value };
    });
  }
};

// Non-admin Cancel SPECIFIC Order (Remove Order)
module.exports.cancelUserSpecificOrder = async (data, reqParams) => {
  if (data.isAdmin == false) {
    return Order.findOneAndUpdate(
      {
        "user.userId": data.id,
        _id: reqParams.orderId,
        isActive: true,
      },
      { isActive: false }
    )
      .then((result) => {
        if (!result) {
          return `Order ID ${reqParams.orderId} does not exist`;
        } else {
          let message = Promise.resolve("Order successfully cancelled");
          return message.then((value) => {
            return { value };
          });
        }
      })
      .catch(() => `${reqParams.orderId} is an invalid Order ID`);
  } else {
    let message = Promise.resolve(
      "User must NOT be admin to cancel user orders"
    );
    return message.then((value) => {
      return { value };
    });
  }
};

// Non-admin Cancel LATEST Order (Remove Order)
module.exports.cancelUserLatestOrder = async (data) => {
  if (data.isAdmin == false) {
    return Order.findOneAndUpdate(
      {
        "user.userId": data.id,
        isActive: true,
      },
      { isActive: false },
      { sort: { $natural: -1 } }
    )
      .then((result) => {
        if (!result) {
          return { value: "No existing orders to cancel" };
        } else {
          return { value: "Order successfully cancelled" };
        }
      })
      .catch(() => {
        let message = Promise.resolve(
          `${reqParams.orderId} is an invalid Order ID`
        );
        return message.then((value) => {
          return { value };
        });
      });
  } else {
    let message = Promise.resolve(
      "User must NOT be admin to cancel user orders"
    );
    return message.then((value) => {
      return { value };
    });
  }
};

// Non-admin Return Total Expenses
module.exports.userTotalExpense = async (data) => {
  if (data.isAdmin == false) {
    return Order.aggregate([
      { $match: { isActive: true, "user.userId": data.id } },
      {
        $group: {
          _id: "$user.userId",
          total: {
            $sum: "$totalAmount",
          },
        },
      },
    ])
      .then((result) => {
        let message = Promise.resolve(
          `The total amount you ordered in this app is now Php ${result[0].total}`
        );
        return message.then((value) => {
          return { value };
        });
      })
      .catch(() => {
        let message = Promise.resolve("No existing orders");
        return message.then((value) => {
          return { value };
        });
      });
  } else {
    let message = Promise.resolve(
      "User must NOT be admin to retrieve user total expense"
    );
    return message.then((value) => {
      return { value };
    });
  }
};

// Return Total expenses of ALL Users (Admin only)
module.exports.allTotalExpenseAdmin = async (data) => {
  if (data.isAdmin) {
    return Order.aggregate([
      { $match: { isActive: true } },
      {
        $group: {
          _id: "$user.userId",
          total: {
            $sum: "$totalAmount",
          },
        },
      },
    ])
      .then((result) => {
        return result;
      })
      .catch(() => {
        let message = Promise.resolve("Retrieving data failed");
        return message.then((value) => {
          return { value };
        });
      });
  } else {
    let message = Promise.resolve("User must be ADMIN to access this");
    return message.then((value) => {
      return { value };
    });
  }
};

// Return quantity of products ordered for authenticated User
module.exports.userTotalQuantityOrdered = async (data) => {
  if (data.isAdmin == false) {
    return Order.aggregate([
      { $match: { isActive: true, "user.userId": data.id } },
      { $unwind: "$products" },
      {
        $group: {
          // _id: "$products.productId",
          _id: "$products.name",
          total: {
            // $sum: "$products.quantity",
            $sum: { $toInt: "$products.quantity" },
          },
        },
      },
    ])
      .then((result) => {
        return result;
      })
      .catch(() => {
        let message = Promise.resolve("Retrieving data failed");
        return message.then((value) => {
          return { value };
        });
      });
  } else {
    ("User must NOT be admin to retrieve user list of quantity of ordered products");
    return message.then((value) => {
      return { value };
    });
  }
};

// Return quantity of products ordered for ALL Users (Admin only)
module.exports.allTotalQuantityOrderedAdmin = async (data) => {
  if (data.isAdmin) {
    return Order.aggregate([
      { $match: { isActive: true } },
      { $unwind: "$products" },
      {
        $group: {
          // _id: "$products.productId",
          _id: "$products.name",
          total: {
            // $sum: "$products.quantity",
            $sum: { $toInt: "$products.quantity" },
          },
        },
      },
    ])
      .then((result) => {
        return result;
      })
      .catch(() => {
        let message = Promise.resolve("Retrieving data failed");
        return message.then((value) => {
          return { value };
        });
      });
  } else {
    let message = Promise.resolve("User must be ADMIN to access this");
    return message.then((value) => {
      return { value };
    });
  }
};

// Non-admin User checkout (Create Order) -- old code
// module.exports.orderProduct = async (data, reqBody) => {
//   if (data.isAdmin == false) {
//     try {
//       const subTotals = [];
//       for (let i = 0; i < reqBody.length; i++) {
//         const subTotal = await Product.findOne({
//           _id: reqBody[i].productId,
//         }).then((res) => {
//           return res.price * reqBody[i].quantity;
//         });
//         subTotals.push(subTotal);
//       }
//       const totalAmount = subTotals.reduce((a, b) => a + b);

//       let newOrder = new Order({
//         user: {
//           userId: data.id,
//           email: data.email,
//         },
//         products: reqBody,
//         subTotals: subTotals,
//         totalAmount: totalAmount,
//       });

//       newOrder.save().then((user, error) => {
//         if (error) {
//           return false;
//         } else {
//           return true;
//         }
//       });

//       let message = Promise.resolve("Order added successfully");
//       return message.then((value) => {
//         return { value };
//       });
//     } catch {
//       return `Invalid Product ID`;
//     }
//   } else {
//     let message = Promise.resolve("User must NOT be admin to add order");
//     return message.then((value) => {
//       return { value };
//     });
//   }
// };

// Retrieve authenticated user’s orders -- old code
// module.exports.getUserOrder = async (data) => {
//   if (data.isAdmin == false) {
//     return (
//       Order.find({ "user.userId": data.id, isActive: true })
//         .populate("products.productId", {
//           price: 0, // hidden because price can be updated but subtotal and total is fixed after ordering, can be confused with wrong computation
//           isActive: 0,
//           createdOn: 0,
//           __v: 0,
//         })
//         // .select({
//         //   _id: 1,
//         //   products: [{ productId: 1, quantity: 1 }],
//         //   subTotals: 1,
//         //   totalAmount: 1,
//         //   purchasedOn: 1,
//         // })
//         .select({
//           user: 0,
//           __v: 0,
//           isActive: 0,
//         })
//         .then((result) => {
//           return result;
//         })
//         .catch(() => `Retrieving order failed`)
//     );
//   } else {
//     let message = Promise.resolve(
//       "User must NOT be admin to retrieve user orders"
//     );
//     return message.then((value) => {
//       return { value };
//     });
//   }
// };
