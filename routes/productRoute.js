const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");
const auth = require("../auth");

// Create Product (Admin only)
router.post("/create", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  productController
    .addProduct(data, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve all active products
router.get("/active", (req, res) => {
  productController
    .getAllActiveProducts()
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve all products for admin
router.get("/all", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  productController
    .getAllProducts()
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve single product
router.get("/:productId", (req, res) => {
  productController
    .getProduct(req.params)
    .then((resultFromController) => res.send(resultFromController));
});

// Update Product information (Admin only)
router.patch("/:productId", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  productController
    .updateProduct(data, req.params, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Archive Product (Admin only)
router.patch("/:productId/archive", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  productController
    .archiveProduct(data, req.params)
    .then((resultFromController) => res.send(resultFromController));
});

// added for capstone 3
// Ractivate Product (Admin only)
router.patch("/:productId/reactivate", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  productController
    .reactivateProduct(data, req.params)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
