const express = require("express");
const router = express.Router();

const cartController = require("../controllers/cartController");
const auth = require("../auth");

// Non-admin User Add to Cart
router.post("/add-to-cart", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    email: auth.decode(req.headers.authorization).email,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  cartController
    .addToCart(data, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Non-admin User Check Out Cart
router.patch("/checkout-cart", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    email: auth.decode(req.headers.authorization).email,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  cartController
    .checkOutCart(data)
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve authenticated user’s cart
router.get("/user-cart", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  cartController
    .getUserCart(data)
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve all cart (Admin only)
router.get("/all-cart", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  cartController
    .getAllCart(data)
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve all orders of SPECIFIC USERID (Admin only)
router.get("/all-cart/:userId", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  cartController
    .getCartSpecificAdmin(data, req.params)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
