const express = require("express");
const router = express.Router();

const orderController = require("../controllers/orderController");
const auth = require("../auth");

// Non-admin User checkout (Create Order)
router.post("/order-product", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    email: auth.decode(req.headers.authorization).email,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  orderController
    .orderProduct(data, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve authenticated user’s orders
router.get("/user-orders", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  orderController
    .getUserOrder(data)
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve authenticated user’s LATEST orders
router.get("/user-latest-order", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  orderController
    .getUserLatestOrder(data)
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve authenticated user’s SPECIFIC orders
router.get("/user-order/:orderId", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  orderController
    .getUserSpecificOrder(data, req.params)
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve all orders (Admin only)
router.get("/all-orders", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  orderController
    .getAllOrders(data)
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve all orders of SPECIFIC USERID (Admin only)
router.get("/all-orders/:userId", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  orderController
    .getAllOrdersSpecificAdmin(data, req.params)
    .then((resultFromController) => res.send(resultFromController));
});

// Non-admin Cancel SPECIFIC Order (Remove Order)
router.patch("/:orderId/cancel", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  orderController
    .cancelUserSpecificOrder(data, req.params)
    .then((resultFromController) => res.send(resultFromController));
});

// Non-admin Cancel LATEST Order (Remove Order)
router.patch("/latest-order-cancel", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  orderController
    .cancelUserLatestOrder(data)
    .then((resultFromController) => res.send(resultFromController));
});

// Non-admin Return Total Expenses
router.get("/user-total-expense", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  orderController
    .userTotalExpense(data)
    .then((resultFromController) => res.send(resultFromController));
});

// Return Total expenses of ALL Users (Admin only)
router.get("/all-total-expense", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  orderController
    .allTotalExpenseAdmin(data)
    .then((resultFromController) => res.send(resultFromController));
});

// Return quantity of products ordered for authenticated User
router.get("/user-quantity-ordered", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  orderController
    .userTotalQuantityOrdered(data)
    .then((resultFromController) => res.send(resultFromController));
});

// Return quantity of products ordered for ALL Users (Admin only)
router.get("/all-quantity-ordered", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  orderController
    .allTotalQuantityOrderedAdmin(data)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
