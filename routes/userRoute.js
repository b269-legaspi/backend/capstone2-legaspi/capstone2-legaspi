const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");
const auth = require("../auth");

// User registration
router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// User authentication
router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve User Details (Authenticated users only)
router.get("/details", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  userController
    .getProfile({ userId: userData.id })
    .then((resultFromController) => res.send(resultFromController));
});

// Set user as admin (Admin only)
router.patch("/:userId/set-user-to-admin", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  userController
    .setUserToAdmin(data, req.params)
    .then((resultFromController) => res.send(resultFromController));
});

// added for capstone 3
// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
  userController
    .checkEmailExists(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve all users (Admin only)
router.patch("/change-pass", auth.verify, (req, res) => {
  const data = {
    id: auth.decode(req.headers.authorization).id,
  };

  userController
    .changePassword(data, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//
router.get("/all-users", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  userController
    .retrieveAllUsers(data)
    .then((resultFromController) => res.send(resultFromController));
});

// Demote admin to user (Admin only)
router.patch("/:userId/demote-admin-to-user", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
  };
  userController
    .demoteAdminToUser(data, req.params)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
