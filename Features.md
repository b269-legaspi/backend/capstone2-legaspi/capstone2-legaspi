(minimum features)
● User registration
● User authentication
● Create Product (Admin only)
● Retrieve all active products
● Retrieve single product
● Update Product information (Admin only)
● Archive Product (Admin only)
● Non-admin User checkout (Create Order)
● Retrieve User Details

(stretch goal)
● Set user as admin (Admin only)
● Retrieve authenticated user’s orders
● Retrieve all orders (Admin only)
● Add to Cart
○○○ Added Products
○○○ Change product quantities
○○○ Remove products from cart
○○○ Subtotal for each item
○○○ Total price for all items

(personal stretch goals)
● Get user latest order
● Get user specific order
● Get specific users and all total expenses (Admin only)
● Get all quantity of products ordered for all users and for specific user (Admin only)
● Cancel user order
● Get specific user's and all users' cart (Admin only)

(added features for full-stack)
● Change password
● Get all users (Admin only)
● Set user to admin (Admin only)
● Demote admin to regular user (Admin only)
● Get all products (active and archived products) (Admin Only)
● Reactivate product (Admin only)
