const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  user: {
    userId: { type: String },
    email: { type: String },
  },
  products: [],
  totalAmount: {
    type: Number,
  },
  purchasedOn: {
    // type: Date,
    type: String,
    default: new Date(),
  },
  purchasedOnUTC: {
    type: Date,
    default: new Date(),
  },
  isActive: {
    type: Boolean,
    default: true,
  },
});

module.exports = mongoose.model("Order", orderSchema);

// old code
// products: [
//   {
//     productId: {
//       type: String,
//     },
//     // productId: {
//     //   type: mongoose.Schema.ObjectId,
//     //   ref: "Product",
//     // },
//     name: {
//       type: String,
//     },
//     description: {
//       type: String,
//     },
//     price: {
//       type: Number,
//     },
//     quantity: {
//       type: Number,
//     },
//     subTotal: {
//       type: Number,
//     },
//   },
// ],
// subTotals: {
//   type: Array,
// },
