const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
  user: {
    userId: { type: String },
    email: { type: String },
  },

  cartProducts: [],

  totalAmount: {
    default: 0,
    type: Number,
  },

  lastModifiedOn: {
    type: String,
    default: new Date(),
  },

  lastModifiedOnUTC: {
    type: Date,
    default: new Date(),
  },
});
module.exports = mongoose.model("Cart", cartSchema);
